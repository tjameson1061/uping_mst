<?php

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class itmedia
{
    var array $response;

    function __construct($client_detail, $post)
    {

        $nextPayDateDay = $post->Employer->nextPayDateDay;
        $nextPayDateMonth = $post->Employer->nextPayDateMonth;
        $nextPayDateYear = $post->Employer->nextPayDateYear;
        $followingPayDateDay = $post->Employer->followingPayDateDay;
        $followingPayDateMonth = $post->Employer->followingPayDateMonth;
        $followingPayDateYear = $post->Employer->followingPayDateYear;

        $AddressMoveIn = $this->AddressMoveIn((string)$post->Residence->monthsAtAddress);
//        Log::debug('AddressMoveIn() Called');
        $rentOwn = $this->rentOwn((string)$post->Residence->residentialStatus);
//        Log::debug('rentOwn() Called');

        $callTime = $this->callTime((string)$post->Additionals->bestTimeToCall); // #
//        Log::debug('callTime() Called');
        $armedForces = $this->armedForces((string)$post->Applicant->inMilitary); //#
//        Log::debug('armedForces() Called');
        $incomeSource = $this->incomeSource((string)$post->Employer->incomeSource); //#
//        Log::debug('incomeSource() Called');
        $EmploymentStarted = $this->EmploymentStarted((string)$post->Employer->monthsAtEmployer); //#
//        Log::debug('EmploymentStarted() Called');
        $monthsAtBank = $this->monthsBank((string)$post->Bank->monthsAtBank); //#
//        Log::debug('monthsBank() Called');
        $incomeCycle = $this->incomeCycle((string)$post->Employer->incomeCycle); // #
//        Log::debug('incomeCycle() Called',(array)$post->Employer->incomeCycle);
        $nextPayday = $this->date_format($nextPayDateDay, $nextPayDateMonth, $nextPayDateYear);
//        Log::debug('date_format() Called');
        $followingPayDate = $this->date_format($followingPayDateDay, $followingPayDateMonth, $followingPayDateYear);
//        Log::debug('date_format() Called');
        $bankAccountType = $this->bankAccountType((string)$post->Bank->bankAccountType); //#
//        Log::debug('bankAccountType() Called');
        $directDeposit = $this->directDeposit($post->Employer->incomePaymentType); // #
//        Log::debug('directDeposit() Called');
        $loanPurpose = $this->loanPurpose((string)$post->Applicant->loanPurpose); //#
//        Log::debug('loanPurpose() Called');
        $creditType = $this->creditType((string)$post->Additionals->creditScore); // #
//        Log::debug('creditType() Called');

        $application = (object)[];
        $application->username = (string)$client_detail->parameter1; //'Amikaro_ToptoBottom';
        $application->apikey = (string)$client_detail->parameter2; //'f4aa37b1e259134058ce85f20ea8d356c97da52c';
        $application->campaignId = (string)$client_detail->parameter3; //'Amikaro_ToptoBottom';
        $application->ip_address = (string)$post->Source->ipAddress;
        $application->agent = (string)$post->Source->userAgent;
        $application->min_price = (string)$client_detail->min_price ?? '';
        $application->amount = (string)$post->Loan->loanAmount;
        $application->fName = (string)$post->Applicant->firstName;
        $application->lName = (string)$post->Applicant->lastName;
        $application->zip = (string)$post->Residence->zip;
        $application->city = (string)$post->Residence->city;
        $application->state = (string)$post->Residence->state;
        $application->address = (string)$post->Residence->addressStreet1;
        $application->lengthAtAddress = (string)$AddressMoveIn;
        $application->licenseState = (string)$post->Applicant->drivingLicenseState;
        $application->email = (string)$post->Applicant->email;
        $application->license = (string)$post->Applicant->drivingLicenseNumber;
        $application->rentOwn = (string)$rentOwn;
        $application->phone = (string)$post->Applicant->cellPhoneNumber;
        $application->workPhone = (string)$post->Applicant->workPhoneNumber;
        $application->callTime = (string)$callTime;
        $application->bMonth = (string)$post->Applicant->dateOfBirthDay;
        $application->bDay = (string)$post->Applicant->dateOfBirthMonth;
        $application->bYear = (string)$post->Applicant->dateOfBirthYear;
        $application->ssn = (string)$post->Applicant->ssn;
        $application->armedForces = (string)$armedForces;
        $application->incomeSource = (string)$incomeSource;
        $application->employerName = (string)$post->Employer->employerName;
        $application->timeEmployed = (string)$EmploymentStarted;
        $application->employerPhone = (string)$post->Applicant->workPhoneNumber;
        $application->jobTitle = (string)$post->Employer->jobTitle;
        $application->paidEvery = (string)$incomeCycle;
        $application->nextPayday = (string)$nextPayday;
        $application->secondPayday = (string)$followingPayDate;
        $application->abaNumber = (string)$post->Bank->bankRoutingNumber;
        $application->accountNumber = (string)$post->Bank->bankAccountNumber;
        $application->accountType = (string)$bankAccountType;
        $application->bankName = (string)$post->Bank->bankName;
        $application->monthsBank = (string)$monthsAtBank;;
        $application->directDeposit = (string)$directDeposit;
        $application->monthlyNetIncome = (string)$post->Employer->monthlyIncome;
        $application->note = (string) 'UPING';
        $application->websiteName = (string)$post->Source->creationUrl;
        $application->timeout = (string)$client_detail->timeout ?? '120';
        $application->loan_reason = (string)$loanPurpose;
        $application->credit_type = (string)$creditType;


        //https://api.itmedia.xyz/post/productionxml/api/v2
        $this->send_application($application);
        Log::debug('send_application() Called', (array)$application);

    }


    private function AddressMoveIn($parameter): int
    {
        $AddressMoveIn = 1;

        switch ($parameter) {
            case 12:
                $AddressMoveIn = 1;
                break;
            case 24:
                $AddressMoveIn = 2;
                break;
            case 36:
                $AddressMoveIn = 3;
                break;
            case 48:
                $AddressMoveIn = 4;
                break;
            case 60:
                $AddressMoveIn = 5;
                break;
            case 72:
                $AddressMoveIn = 6;
                break;
            case 84:
                $AddressMoveIn = 7;
                break;
            case 96:
                $AddressMoveIn = 8;
                break;
        }
        return $AddressMoveIn;
    }

    private function rentOwn($parameter): string
    {
        $rentOwn = 'own';

        switch ($parameter) {
            case 1 || 2 || 3:
                $rentOwn = 'rent';
                break;
            case 4 || 5:
                $rentOwn = 'own';
                break;
        }
        return $rentOwn;

    }

    private function armedForces($parameter): string
    {
        $armedForces = 'no';

        switch ($parameter) {
            case 2 || 3:
                $armedForces = 'yes';
                break;
            case 1:
                $armedForces = 'no';
                break;
        }

        return $armedForces;

    }

    private function date_format($day, $month, $year): string
    {
        return $day . '-' . $month . '-' . $year;
    }

    private function EmploymentStarted($parameter): int
    {
        $EmploymentStarted = 1;

        switch ($parameter) {
            case 12:
                $EmploymentStarted = 1;
                break;
            case 24:
                $EmploymentStarted = 2;
                break;
            case 36:
                $EmploymentStarted = 3;
                break;
            case 48:
                $EmploymentStarted = 4;
                break;
            case 60:
                $EmploymentStarted = 5;
                break;
            case 72:
                $EmploymentStarted = 6;
                break;
            case 84:
                $EmploymentStarted = 7;
                break;
            case 96:
                $EmploymentStarted = 8;
                break;

        }
        return $EmploymentStarted;

    }
    private function monthsBank($parameter): int
    {
        $monthsAtBank = 1;

        switch ($parameter) {
            case 12:
                $monthsAtBank = 1;
                break;
            case 24:
                $monthsAtBank = 2;
                break;
            case 36:
                $monthsAtBank = 3;
                break;
            case 48:
                $monthsAtBank = 4;
                break;
            case 60:
                $monthsAtBank = 5;
                break;
            case 72:
                $monthsAtBank = 6;
                break;
            case 84:
                $monthsAtBank = 7;
                break;
            case 96:
                $monthsAtBank = 8;
                break;

        }
        return $monthsAtBank;

    }
    private function loanPurpose($parameter): string
    {

        $loanPurpose = 'other';

        switch ($parameter) {
            case 1:
                $loanPurpose = 'debtConsolidation';
                break;
            case 2:
                $loanPurpose = 'emergencySituation';
                break;
            case 3:
                $loanPurpose = 'autoRepair';
                break;
            case 4:
                $loanPurpose = 'autoPurchase';
                break;
            case 5:
                $loanPurpose = 'moving';
                break;
            case 6:
                $loanPurpose = 'homeImprovement';
                break;
            case 7:
                $loanPurpose = 'medical';
                break;
            case 8:
                $loanPurpose = 'business';
                break;
            case 9:
                $loanPurpose = 'vacation';
                break;
            case 10:
                $loanPurpose = 'taxes';
                break;
            case 11:
                $loanPurpose = 'rentOrMortgage';
                break;
            case 12:
                $loanPurpose = 'wedding';
                break;
            case 13:
                $loanPurpose = 'majorPurchase';
                break;
            case 14:
                $loanPurpose = 'other';
                break;

        }
        return $loanPurpose;
    }

    private function callTime($parameter): string
    {
        $callTime = 'anytime';

        switch ($parameter) {
            case 1:
                $callTime = 'anytime';
                break;
            case 2:
                $callTime = 'morning';
                break;
            case 3:
                $callTime = 'afternoon';
                break;
            case 4:
                $callTime = 'evening';
                break;
        }
        return $callTime;
    }

    private function incomeSource($parameter): string
    {
        $incomeSource = 'employment';

        switch ($parameter) {
            case 1:
                $incomeSource = 'employment';
                break;
            case 2:
                $incomeSource = 'selfemployment';
                break;
            case 3:
                $incomeSource = 'benefits';
                break;
            case 4:
                $incomeSource = 'unemployed';
                break;
        }
        return $incomeSource;
    }

    private function incomeCycle($parameter): string
    {
        $incomeCycle = 'biweekly';

        switch ($parameter) {
            case '1':
                $incomeCycle = 'biweekly';
                break;
            case '2':
                $incomeCycle = 'weekly';
                break;
            case '3':
                $incomeCycle = 'monthly';
                break;
            case '4':
                $incomeCycle = 'twicemonthly';
                break;
        }
        return $incomeCycle;

    }

    private function bankAccountType($parameter): string
    {
        $bankAccountType = 'checking';

        switch ($parameter) {
            case 5:
                $bankAccountType = 'checking';
                break;
            case 6:
                $bankAccountType = 'saving';
                break;
        }

        return $bankAccountType;

    }

    private function directDeposit($parameter): string
    {
        $directDeposit = 'yes';

        switch ($parameter) {
            case 1:
                $directDeposit = 'yes';
                break;
            case 2 || 3:
                $directDeposit = 'no';
                break;
        }
        return $directDeposit;

    }

    private function creditType($parameter): string
    {
        $creditType = 'good';

        switch ($parameter) {
            case 1:
                $creditType = 'excellent';
                break;
            case 2:
                $creditType = 'good';
                break;
            case 3:
                $creditType = 'fair';
                break;
            case 4:
                $creditType = 'poor';
                break;
            case 5:
                $creditType = 'notSure';

        }
        return $creditType;

    }


    private function send_application($application)
    {
        $output = Http::asForm()->post('https://api.itmedia.xyz/post/testjson/api/v2', (array)$application);
        $application_status = $output->object();
        Log::debug('ITMEDIA API RESPONSE', (array)$application_status);
        $this->response['application_response'] = $application_status;
    }

    public function returnresponse()
    {
        $appResponse = $this->response['application_response'];

        if ($appResponse->Status == 'Sold') {
            $this->response['accept'] = 'ACCEPTED';
            $this->response['post_price'] = $appResponse->Price;
            $this->response['post_status'] = '1';
            $this->response['redirect_url'] = $appResponse->Redirect;
            $this->response['post_time'] = '0';


        } elseif ($appResponse->Status == 'Rejected') {
            $this->response['accept'] = 'REJECTED';
            $this->response['post_status'] = '0';
            $this->response['post_price'] = '0';
            $this->response['post_time'] = '0';


        } else {
            $this->response['errors'] = 'Validation Failed';
            $this->response['post_status'] = '0';
            $this->response['post_price'] = '0';
            $this->response['post_time'] = '0';

        }
        $this->response['post_time'] = $appResponse->post_time ?? '';

        return $this->response;
    }
}


