<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LmsPartnerLeadtype extends Model
{
    use HasFactory;

    protected $table = 'lms_partner_leadtype';
}
